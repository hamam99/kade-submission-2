package com.codebreaker.submission2

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService
{
    @GET("api/v1/json/{token}/eventspastleague.php")
    fun getPastEventLeague(
            @Path("token") token: String,
            @Query("id") idLeague: Int
    ): Observable<Response<EventMdl>>

    @GET("api/v1/json/{token}/eventsnextleague.php")
    fun getNextEventLeague(
            @Path("token") token: String,
            @Query("id") idLeague: Int
    ): Observable<Response<EventMdl>>

    @GET("api/v1/json/{token}/lookupteam.php")
    fun getDetailTeam(
            @Path("token") token: String,
            @Query("id") idTeam: Int
    ): Observable<Response<LookupTeams>>

}
