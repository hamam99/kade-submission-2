package com.codebreaker.submission2

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class EventAdapter(private val items: List<EventMdl.EventsBean>, private val listener: (EventMdl.EventsBean) -> Unit)
    : RecyclerView.Adapter<EventAdapter.ViewHolder>()
{


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_footbal, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        holder.bindItem(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val date: TextView = view?.findViewById(R.id.item_date)
        val nameHome: TextView = view?.findViewById(R.id.name_home)
        val nameAway: TextView = view?.findViewById(R.id.name_away)
        val skorHome: TextView = view?.findViewById(R.id.home_skor)
        val skorAway: TextView = view?.findViewById(R.id.away_skor)


        fun bindItem(items: EventMdl.EventsBean, listener: (EventMdl.EventsBean) -> Unit)
        {
            date.text = formatDates(items.dateEvent.toString())
            nameHome.text = items.strHomeTeam
            nameAway.text = items.strAwayTeam
            skorHome.text = items.intHomeScore
            skorAway.text = items.intAwayScore

            itemView.setOnClickListener {
                listener(items)
            }
        }
    }

    private fun formatDates(date: String): String
    {
        val input = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val output = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        var hasil = ""
        try
        {
            val oneWayTripDate = input.parse(date)
            hasil = output.format(oneWayTripDate)
        } catch (e: ParseException)
        {
            e.printStackTrace()
        }

        return hasil
    }

}

