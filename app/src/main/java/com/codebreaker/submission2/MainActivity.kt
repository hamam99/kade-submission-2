package com.codebreaker.submission2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity()
{

    val PAST_EVENT_ID = 4328
    private val NEXT_EVENT_ID = 4328
    private var listEvent: MutableList<EventMdl.EventsBean> = ArrayList<EventMdl.EventsBean>()
    lateinit var adapter: EventAdapter

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadPrevMatch()

        navigationView?.setOnNavigationItemSelectedListener {
            when (it.itemId)
            {
                R.id.prevMatch ->
                {
                    loadPrevMatch()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.nextMatch ->
                {
                    loadNextMatch()
                    return@setOnNavigationItemSelectedListener true
                }


            }
            return@setOnNavigationItemSelectedListener false
        }


        adapter = EventAdapter(listEvent) {
            EventMdl.EventObj.eventObj = it
            val intent = Intent(this, DetailActivity::class.java)
            startActivity(intent)
        }
        recyclerView.adapter = adapter


        swipeRefreshlayout?.setOnRefreshListener {
            val menu = navigationView?.selectedItemId
            when (menu)
            {
                R.id.prevMatch ->
                {
                    loadPrevMatch()
                }
                R.id.nextMatch ->
                {
                    loadNextMatch()
                }
            }

        }
    }

    @SuppressLint("CheckResult")
    private fun loadPrevMatch()
    {
        showRefreshLayout()

        ApiManager.getPastEvent(PAST_EVENT_ID)
                .doOnComplete {
                    hideRefreshLayout()
                }
                .subscribe(
                        { response ->
                            when (response.code())
                            {
                                200 ->
                                {
                                    val listResult = response.body()?.events
                                    listEvent.clear()
                                    listEvent.addAll(listResult!!)
                                    adapter?.notifyDataSetChanged()

                                }
                                else ->
                                {
                                    throw  error("maaf terjadi kesalahan!")
                                }
                            }

                        },
                        { error ->
                            error.printStackTrace()
                            Toast.makeText(this, error.localizedMessage, Toast.LENGTH_SHORT).show()
                            hideRefreshLayout()
                        }
                )
    }

    @SuppressLint("CheckResult")
    private fun loadNextMatch()
    {
        showRefreshLayout()

        ApiManager.getNextEvent(NEXT_EVENT_ID)
                .doOnComplete {
                    hideRefreshLayout()
                }
                .subscribe(
                        { response ->
                            when (response.code())
                            {
                                200 ->
                                {
                                    val listResult = response.body()?.events
                                    listEvent.clear()
                                    listEvent.addAll(listResult!!)
                                    adapter?.notifyDataSetChanged()

                                }
                                else ->
                                {
                                    throw  error("maaf terjadi kesalahan!")
                                }
                            }

                        },
                        { error ->
                            error.printStackTrace()
                            Toast.makeText(this, error.localizedMessage, Toast.LENGTH_SHORT).show()
                            hideRefreshLayout()
                        }
                )
    }

    private fun showRefreshLayout()
    {
        swipeRefreshlayout?.isRefreshing = true
    }

    private fun hideRefreshLayout()
    {
        swipeRefreshlayout?.isRefreshing = false
    }


}
