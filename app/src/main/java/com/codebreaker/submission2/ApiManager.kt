package  com.codebreaker.submission2

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by akira on 14/11/17.
 */

object ApiManager
{

    private const val MAIN_URL: String = "https://www.thesportsdb.com/"

    private lateinit var mainApiService: ApiService

    init
    {
        val retrofit = initRetrofit(MAIN_URL)
        initMainServices(retrofit)
    }

    private fun initRetrofit(url: String): Retrofit
    {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val client = OkHttpClient.Builder().apply {
            networkInterceptors().add(Interceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                        .header("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build()
                chain.proceed(request)
            })
            addInterceptor(interceptor)
        }
        client.readTimeout(2000000, TimeUnit.MILLISECONDS)
        client.retryOnConnectionFailure(true)


        return Retrofit.Builder().baseUrl(url)
                .addConverterFactory(createMoshiConverter())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .build()
    }

    private fun createMoshiConverter(): MoshiConverterFactory = MoshiConverterFactory.create()

    private fun initMainServices(retrofit: Retrofit)
    {
        mainApiService = retrofit.create(ApiService::class.java)
    }


    fun getPastEvent(idLeague: Int) =
            mainApiService.getPastEventLeague(BuildConfig.ApiKey, idLeague)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun getNextEvent(idLeague: Int) =
            mainApiService.getNextEventLeague(BuildConfig.ApiKey, idLeague)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun getDetailTeams(idTeams: Int) =
            mainApiService.getDetailTeam(BuildConfig.ApiKey, idTeams)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())


}
