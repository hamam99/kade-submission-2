package com.codebreaker.submission2

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DetailActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        setView(EventMdl.EventObj.eventObj)
    }

    private fun setView(eventObj: EventMdl.EventsBean)
    {
        detailTitle.text = eventObj.strEvent
        date.text = formatDates(eventObj.dateEvent.toString())

        name_home.text = eventObj.strHomeTeam
        name_away.text = eventObj.strAwayTeam
        home_skor.text = eventObj.intHomeScore
        away_skor.text = eventObj.intAwayScore

        getLogo(eventObj.idAwayTeam.toString(),img_away)
        getLogo(eventObj.idHomeTeam.toString(),img_home)

    }

    private fun formatDates(date: String): String
    {
        val input = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val output = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        var hasil = ""
        try
        {
            val oneWayTripDate = input.parse(date)
            hasil = output.format(oneWayTripDate)
        } catch (e: ParseException)
        {
            e.printStackTrace()
        }

        return hasil
    }

    @SuppressLint("CheckResult")
    private fun getLogo(idTeams: String, imageView: ImageView)
    {
        ApiManager.getDetailTeams(idTeams.toInt())
                .doOnComplete {
                }
                .subscribe(
                        { response ->
                            when (response.code())
                            {
                                200 ->
                                {
                                    val result = response.body()
                                    if(result?.teams!!.isNotEmpty()){
                                        val url = result?.teams!!.get(0)?.strTeamBadge
                                        Glide.with(this).load(url).into(imageView);
                                    }

                                }
                                else ->
                                {
                                    throw  error("maaf terjadi kesalahan!")
                                }
                            }

                        },
                        { error ->
                            error.printStackTrace()
                            Toast.makeText(this, error.localizedMessage, Toast.LENGTH_SHORT).show()
                        }
                )
    }

}
