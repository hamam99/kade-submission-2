package com.codebreaker.submission2

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class EventMdl
    : Parcelable
{

    var events: List<EventsBean>? = null

    class EventsBean
    {
        var strEvent: String? = null
        var strHomeTeam: String? = null
        var strAwayTeam: String? = null
        var intHomeScore: String? = null
        var intAwayScore: String? = null
        var dateEvent: String? = null
        var idHomeTeam: String? = null
        var idAwayTeam: String? = null
    }

    object EventObj
    {
        var eventObj = EventMdl.EventsBean()
    }
}